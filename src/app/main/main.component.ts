import { Component, OnInit } from '@angular/core';
import { DataService } from '../utils/services/data.service';
import { ImageC } from '../utils/classes/ImageC';
import {UiService} from '../utils/services/ui.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  // array to hold images from request
  public imagesList: ImageC[] = [];
  // hold images favorite list
  public imagesFavoriteList: ImageC[] = [];
  public showFavorites = false;
  constructor(private dataService: DataService, private uiService: UiService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataService.getImagesRequest().subscribe(
      data => {
        this.imagesList = data;
        // check session storage and load images
        this.imagesFavoriteList = JSON.parse(sessionStorage.getItem('favlist'));
        // mark the images in imagesList if they exist
        if (this.imagesFavoriteList) {
          this.imagesFavoriteList.forEach((item) => {
            this.markImage(item.url);
          });
        } else {
          this.imagesFavoriteList = [];
        }
      });
    this.uiService.sidebarShowFavoritesSubject.subscribe(
      data => this.showOnlyFavorites()
    );
  }
  imagesFavoriteListContainsImage(imgSrc): boolean {
    return this.imagesFavoriteList.filter(item => {
      return item.url === imgSrc;
    }).length > 0;
  }
  markImage(imgSrc) {
    this.imagesList.forEach((item) => {
      if (item.url === imgSrc) {
        console.log('it its');
        item.isFavored = true;
      }
    });
  }
  findImage(imgSrc): ImageC {
    return this.imagesList.filter(item => {
      return item.url === imgSrc;
    })[0];
  }
  onImageClick(e): void {
    const tmpImage: ImageC = this.findImage(e.src);
    // if isFavored is clicked means that we need to remove it from favorites array
    if (tmpImage.isFavored) {
      this.imagesFavoriteList = this.imagesFavoriteList.filter(item => {
        return item.url !== e.src;
      });
      this.snackBar.open('Image removed from favorite list', 'Image', {duration: 3000});
    } else {
      // if it's not already in favorite list
      if (!this.imagesFavoriteListContainsImage(e.src)) {
        this.imagesFavoriteList.push(
          new ImageC(e.src, e.height, e.width, true)
        );
        this.snackBar.open('Image added to favorite list', 'Image', {duration: 3000});
      }
    }
    // toggle fav icon for this image
    tmpImage.isFavored = !tmpImage.isFavored;
    // save favorite list to local storage
    sessionStorage.setItem('favlist', JSON.stringify(this.imagesFavoriteList));
  }
  showOnlyFavorites(): void {
    this.showFavorites = !this.showFavorites;
  }

}
