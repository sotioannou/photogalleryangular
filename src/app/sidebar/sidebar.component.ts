import { Component, OnInit, Input } from '@angular/core';
import {UiService} from '../utils/services/ui.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  constructor(private uiService: UiService) { }

  ngOnInit() {
  }
  toggleFavorites() {
    this.uiService.showFavorites();
  }
}
