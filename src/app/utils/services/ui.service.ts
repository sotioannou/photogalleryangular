import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class UiService {
  public sidebarTogglerSubject;
  public sidebarShowFavoritesSubject;
  constructor() {
    this.sidebarTogglerSubject = new Subject();
    this.sidebarShowFavoritesSubject = new Subject();
  }
  toggleSideBar() {
    this.sidebarTogglerSubject.next('toggle');
  }
  showFavorites() {
    this.sidebarShowFavoritesSubject.next('showFavorites');
  }
}
