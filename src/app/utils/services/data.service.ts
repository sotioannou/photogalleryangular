import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {ImageC} from '../classes/ImageC';

@Injectable()
export class DataService {
  private imagesUrl = 'https://pixabay.com/api/';
  private apiKey = '6494743-7947e11d8ea5a4e4b9ad1bb25';
  constructor(private http: HttpClient) {
  }
  getImagesRequest(): Observable <any> {
    return this.http.get(this.imagesUrl + '?key=' + this.apiKey + '&q=dogs').map((response: any) => {
      return response.hits.map(item => {
        return new ImageC(item.webformatURL,  item.webformatHeight, item.webformatWidth);
      });
    });
  }
}
