export class ImageC {
  constructor(public url: string, public height: string, public width: string, public isFavored: boolean = false) {}
}
