import { Component, OnInit, ViewChild } from '@angular/core';
import {UiService} from './utils/services/ui.service';
import 'hammerjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('sidenav') private sidenav;
  constructor(private uiservice: UiService) { }
  ngOnInit() {
    this.subscribeToSidebarToggler();
  }
  subscribeToSidebarToggler() {
    this.uiservice.sidebarTogglerSubject.subscribe(
      data => this.sidenav.toggle()
    );
  }
}
