import {Component, OnInit, Input, Output, EventEmitter, ViewChild} from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {
  @Input() private url: string;
  @Input() private height: string;
  @Input() private width: string;
  @Input() private isFavorite: boolean;
  @Output() private favoriteToggle;
  @ViewChild('img')private img;
  private favoriteIconClicked = false;
  constructor() {
    this.favoriteToggle = new EventEmitter();
  }

  ngOnInit() {
    if (this.isFavorite) {
      this.favoriteIconClicked = !this.favoriteIconClicked;
    }
  }
  onFavoriteIconClick(e) {
    this.favoriteIconClicked = !this.favoriteIconClicked;
    this.favoriteToggle.emit(this.img.nativeElement);
  }


}
