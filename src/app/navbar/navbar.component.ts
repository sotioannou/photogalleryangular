import { Component, OnInit } from '@angular/core';
import {UiService} from '../utils/services/ui.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private uiservice: UiService) { }

  ngOnInit() {
  }
  onSideBarButtonToggle() {
    this.uiservice.toggleSideBar();
  }
}
